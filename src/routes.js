import VolumeView from "./pages/VolumeView";
import EpisodeView from "./pages/EpisodeView";
import Homepage from "./pages/Homepage";
import ChapterView from "./pages/ChapterView";

const routes = [
    {
        path: "/episode-:episode([1-8])/volume-:volume([1-9])",
        component: VolumeView,
    },
    {
        path: "/episode-:episode([1-8])",
        component: EpisodeView,
    },
    {
        // FIXME: Url can finish with "page-"
        path: "/episode-:episode([1-8])/chapter-:chapter([1-9]\\d*)(/page-)?:page([1-9]\\d*)?",
        component: ChapterView,
    },
    {
        path: "",
        component: Homepage,
    },
]

export default routes;