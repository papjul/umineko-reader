import ep1 from "./ep1";
import ep2 from "./ep2";
import ep3 from "./ep3";
import ep4 from "./ep4";
import ep5 from "./ep5";
import ep6 from "./ep6";
import ep7 from "./ep7";
import ep8 from "./ep8";

const episodes = {
    1: ep1, 2: ep2, 3: ep3, 4: ep4,
    5: ep5, 6: ep6, 7: ep7, 8: ep8
};

export default episodes;