import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    app_name: {
        marginRight: theme.spacing(2),
        textDecoration: "none"
    },
}));

const TopMobile = ({pageTitle}) => {
    const classes = useStyles();

    return (
        <AppBar position="sticky">
            <Toolbar>
                <Typography variant="h6" color="inherit" noWrap component={Link}
                            to="" className={classes.app_name}
                >
                    {pageTitle ? pageTitle : "Umineko no Naku Koro ni"}
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

TopMobile.propTypes = {
    pageTitle: PropTypes.string
};

export default TopMobile;