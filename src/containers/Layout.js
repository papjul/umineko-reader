import React from "react";

import Container from "@material-ui/core/Container";
import TopDesktop from "./TopDesktop";
import TopMobile from "./TopMobile";
import Hidden from "@material-ui/core/Hidden";
//import {Helmet} from "react-helmet";
import {makeStyles} from "@material-ui/core/styles";
//import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles(theme => ({
    container: {
        paddingTop: theme.spacing(3)
    },
    javascriptAlert: {
        marginBottom: theme.spacing(3)
    }
}));

const Layout = ({children, pageTitle}) => {
    const classes = useStyles();

    return (
        <>
            {/*<Helmet>
                <title>{pageTitle ? pageTitle : "Umineko no Naku Koro ni"}</title>
            </Helmet>*/}

            <Hidden mdDown implementation="css">
                <TopDesktop />
            </Hidden>

            <Hidden lgUp implementation="css">
                <TopMobile{...{pageTitle}} />
            </Hidden>

            <Container className={classes.container}>
                {/*<noscript>
                    <Alert severity="info" className={classes.javascriptAlert}>
                        Vous avez désactivé JavaScript et nous respectons ce choix.<br />
                        Toutefois, l’expérience sur le site sera très limitée. Pour vous convaincre de mettre ce site
                        sur
                        liste blanche, nous tenions à ce que vous sachiez que rien de malveillant n’est effectué en
                        JavaScript et aucun site tiers consulté, le code source pouvant être librement audité.<br />
                        Par ailleurs, sur l’aspect performances, la majorité du JavaScript est généré par le serveur, ne
                        laissant qu’une partie mineure à votre navigateur à prendre en compte.
                    </Alert>
                </noscript>*/}
                {children}
            </Container>
        </>
    );
};

export default Layout;
