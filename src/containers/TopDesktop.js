import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    app_name: {
        marginRight: theme.spacing(2),
        textDecoration: "none"
    },
}));

const TopDesktop = () => {
    const classes = useStyles();

    return (
        <AppBar position="relative">
            <Toolbar>
                <Typography variant="h6" color="inherit" noWrap component={Link}
                            to="" className={classes.app_name}
                >
                    Umineko no Naku Koro ni
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

export default TopDesktop;