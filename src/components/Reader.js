import React, {useEffect, useRef, useState} from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core/styles";
import {useHistory, useParams} from "react-router-dom";
//import _ from "lodash";
import Layout from "../containers/Layout";

import {TransformWrapper, TransformComponent} from "react-zoom-pan-pinch";
/*
 TODO:
 - Cursor must become a "Left" arrow and a "Right" arrow on desktop
 - Preload next image
 - Preload next audio?
 */

const useStyles = makeStyles(theme => ({
    container: {
        height: "100vh",
        maxWidth: "100%",
        background: "#000",
        cursor: "pointer"
    },
    img: {
        maxHeight: "100vh",
        maxWidth: "100%",
        flexShrink: 1
    },
    actionButtons: {
        "& > button": {
            backgroundColor: "rgba(0, 0, 0, 0.45)",
            /*borderRadius: 2,
            color: "#999",
            display: "block",
            fontSize: 22,
            marginTop: -10,
            padding: "8px 10px 9px",*/
            position: "absolute",
            /*top: "50%",
            zIndex: 1080,
            border: "none",
            outline: "none",*/
            //opacity: 1
        }
    },
    previousAction: {
        /*right: 20,
        left: "initial",
        "&::after": {
            content: "'>'"
        }*/
    },
    nextAction: {
        /*right: "initial",
        left: 20,
        "&::before": {
            content: "'<'"
        }*/
    }
}));

const Reader = ({script}) => {
    const history = useHistory();
    const classes = useStyles();
    const params = useParams();

    //const ZoomComponent = window.Touch ? PinchToZoom : React.Fragment;
    /*const swipeHandlers = useSwipeable({
        onSwiped: (eventData) => {
            if (eventData.dir === LEFT) {
                if ("current" in backRef) {
                    backRef.current.click();
                }
            }
            if (eventData.dir === RIGHT) {
                if ("current" in nextRef) {
                    nextRef.current.click();
                }
            }
        }
    });*/

    const [episode, setEpisode] = useState(parseInt(params.episode));
    const [chapter, setChapter] = useState(parseInt(params.chapter));
    const [currentPage, setCurrentPage] = useState(parseInt(params.page) - 1 || 0);
    const [hasNextChapter, setHasNextChapter] = useState(false);

    const [bgm, setBgm] = useState(null);
    const [se, setSe] = useState([]);
    const [voice, setVoice] = useState(null);

    const backRef = useRef(null);
    const nextRef = useRef(null);

    const handleBack = () => {
        if (currentPage > 0) {
            setCurrentPage(currentPage - 1);
            history.push("/episode-" + episode + "/chapter-" + chapter + "/page-" + currentPage);
        } else if (chapter > 1) {
            setCurrentPage(script.chapters[chapter - 1].length - 1);
            setChapter(chapter - 1);
            history.push("/episode-" + episode + "/chapter-" + (chapter - 1) + "/page-" + script.chapters[chapter - 1].length);
        } /*else if (episode > 1) {
            setEpisode(episode - 1);
            setChapter(_.size(episodes[episode - 1].chapters));
            setCurrentPage(episodes[episode - 1].chapters[_.size(episodes[episode - 1].chapters)].length - 1);
            history.push("/episode-" + (episode - 1) + "/chapter-" + _.size(episodes[episode - 1].chapters) + "/page-" + episodes[episode - 1].chapters[_.size(episodes[episode - 1].chapters)].length);
        }*/
    };

    const handleNext = () => {
        if ((currentPage + 1) in script.chapters[chapter].pages) {
            setCurrentPage(currentPage + 1);
            history.push("/episode-" + episode + "/chapter-" + chapter + "/page-" + (currentPage + 2));
        } else if (hasNextChapter) {
            setCurrentPage(0);
            setChapter(chapter + 1);
            history.push("/episode-" + episode + "/chapter-" + (chapter + 1));
        } else if (episode <= 8) {
            //setCurrentPage(0);
            //setChapter(1);
            //setEpisode(episode + 1);
            //history.push("/episode-" + (episode + 1) + "/chapter-1");
            history.push("/episode-" + (episode + 1));
        }
    };

    useEffect(() => {
        setHasNextChapter((chapter + 1) in script.chapters);
    }, [episode, chapter]);

    useEffect(() => {
        if (script.chapters[chapter].pages[currentPage].bgm) {
            // Avoid reloading BGM from start if it is already playing
            if (!(bgm && bgm.src && bgm.src.includes("/assets/common/bgm/umib_" + script.chapters[chapter].pages[currentPage].bgm + ".ogg"))) {
                let tmpBgm = new Audio("/assets/common/bgm/umib_" + script.chapters[chapter].pages[currentPage].bgm + ".ogg");
                tmpBgm.loop = true;
                tmpBgm.volume = 0.8;
                tmpBgm.play();
                setBgm(tmpBgm);
            }
        } else {
            setBgm(null);
        }

        if (se && Array.isArray(se) && se.length) {
            setSe(playingSe => {
                if (script.chapters[chapter].pages[currentPage].se && script.chapters[chapter].pages[currentPage].se.length) {
                    // Remove SE not playing anymore on the new page
                    for (let currentPlaySe of playingSe) {
                        let found = false;
                        for (let currentPageSe of script.chapters[chapter].pages[currentPage].se) {
                            if (currentPlaySe.src.includes("/assets/common/se/umilse_" + currentPageSe + ".ogg")) {
                                found = true;
                            }
                        }
                        if (!found) {
                            currentPlaySe.pause();
                            currentPlaySe.currentTime = 0;
                            playingSe.splice(playingSe.indexOf(currentPlaySe), 1);
                        }
                    }

                    // Add SE that were not playing before
                    for (let currentPageSe of script.chapters[chapter].pages[currentPage].se) {
                        let found = false;
                        for (let currentPlaySe of se) {
                            if (currentPlaySe.src.includes("/assets/common/se/umilse_" + currentPageSe + ".ogg")) {
                                found = true;
                            }
                        }
                        if (!found) {
                            let tmpSe = new Audio("/assets/common/se/umilse_" + currentPageSe + ".ogg");
                            tmpSe.loop = true;
                            tmpSe.volume = 0.8;
                            tmpSe.play();
                            playingSe.push(tmpSe);
                        }
                    }
                } else {
                    //console.log("Removing all playing SE");
                    for (let currentPlaySe of playingSe) {
                        currentPlaySe.pause();
                        currentPlaySe.currentTime = 0;
                        playingSe.splice(playingSe.indexOf(currentPlaySe), 1);
                    }
                }
                return playingSe;
            });
        } else {
            setSe(script.chapters[chapter].pages[currentPage].se.map(seAudio => {
                let tmpSe = new Audio("/assets/common/se/umilse_" + seAudio + ".ogg");
                tmpSe.loop = true;
                tmpSe.volume = 0.8;
                tmpSe.play();
                return tmpSe;
            }));
        }

        if (script.chapters[chapter].pages[currentPage].voice) {
            let tmpVoice = new Audio("/assets/ep-" + episode + "/voice/ch-" + chapter + "/" + script.chapters[chapter].pages[currentPage].page + ".ogg");
            tmpVoice.play();
            setVoice(tmpVoice);
        } else {
            setVoice(null);
        }
    }, [currentPage, episode, chapter]);

    useEffect(() => {
        const onKeydown = (e) => {
            if (e.key === "ArrowRight" || e.key === "ArrowDown") {
                if ("current" in backRef) {
                    backRef.current.click();
                }
            } else if (e.key === "ArrowLeft" || e.key === "ArrowUp") {
                if ("current" in nextRef) {
                    nextRef.current.click();
                }
            }
        };

        document.addEventListener("keydown", onKeydown);
        return () => { // Remove event listeners on cleanup
            document.removeEventListener("keydown", onKeydown);
        };
    }, [backRef, nextRef]);

    useEffect(() => {
        return () => {
            if (bgm) {
                bgm.pause();
                bgm.currentTime = 0;
            }
        };
    }, [bgm]);

    useEffect(() => {
        return () => {
            if (voice) {
                voice.pause();
                voice.currentTime = 0;
            }
        };
    }, [voice]);

    const handleContainerClick = (e) => {
        // FIXME: Event is called twice, once with clientX = 0, because of the trigerring of click in our own function below
        // Investigate how we can fix this
        if (e.clientX !== 0) {
            if (e.clientX < document.body.clientWidth / 2) {
                if ("current" in nextRef) {
                    nextRef.current.click();
                }
            } else {
                if ("current" in backRef) {
                    backRef.current.click();
                }
            }
        }
    };

    return (chapter in script.chapters && 'pages' in script.chapters[chapter] && currentPage in script.chapters[chapter].pages) ? (
        <>
            <CssBaseline />
            <Box display="flex" justifyContent="center" alignItems="center" flexDirection="column"
                 className={classes.container} onClick={(e) => handleContainerClick(e)}>{/*...swipeHandlers*/}
                <TransformWrapper pan={{lockAxisX: true, lockAxisY: true}}>
                    <TransformComponent>
                        <img className={classes.img} alt=""
                             src={"/assets/ep-" + episode + "/img/ch-" + chapter + "/" + script.chapters[chapter].pages[currentPage].page + ".jpg"}
                        />
                    </TransformComponent>
                </TransformWrapper>
                <div className={classes.actionButtons}>
                    <button aria-label="Previous slide" className={classes.previousAction}
                            onClick={() => handleBack()} ref={backRef} />
                    <button aria-label="Next slide" className={classes.nextAction}
                            onClick={() => handleNext()} ref={nextRef} />
                </div>
            </Box>
        </>
    ) : <Layout>
        <div>Chargement…</div>
    </Layout>;
};

export default Reader;