import {createMuiTheme} from "@material-ui/core/styles";
import red from "@material-ui/core/colors/red";
import {frFR} from "@material-ui/core/locale";

const theme = createMuiTheme({
    palette: {
        primary: {
            //main: '#71322f'
            main: '#AA4336'
        },
        //secondary: deepPurple,
        error: red,
        type: "dark",
    },
}, frFR);

export default theme;