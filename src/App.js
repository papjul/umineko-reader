import React from "react";
import {BrowserRouter} from "react-router-dom";
import theme from "./theme";
import {ThemeProvider} from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import {Route, Switch} from "react-router-dom";
import routes from "./routes";

const App = () => {
    return (
        <BrowserRouter>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Switch>
                    {routes.map((route, idx) => {
                            // TODO: loadable
                            return (
                                <Route key={idx} path={route.path} exact
                                       component={route.component} />
                            );
                        }
                    )}

                    {/*<Route status={404} render={() => <PageNotFound {...{actions, config}} />} />*/}
                </Switch>
            </ThemeProvider>
        </BrowserRouter>
    );
};

export default App;
