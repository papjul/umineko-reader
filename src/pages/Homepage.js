import React from "react";
import Layout from "../containers/Layout";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import {Link as MuiLink} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";

const UminekoCover = ({episode, volume}) => (
    <Link to={"/episode-" + episode + "/volume-" + volume}>
        <img src={"/img/ep-" + episode + "/vol-" + volume + "/cover.jpg"} alt="" />
    </Link>
);

const useStyles = makeStyles((theme) => ({
    gridList: {
        marginBottom: theme.spacing(3) + "px !important",
        "& img": {
            maxHeight: 300
        }
    }
}));

const Homepage = () => {
    const classes = useStyles();
    return (
        <Layout>
            <Typography component="h1" variant="h3">Umineko no <span style={{color: "#91403c"}}>Na</span>ku Koro ni |
                Le <span style={{color: "#91403c"}}>sang</span>lot
                des goélands</Typography>
            <p>Bienvenue sur cette expérience interactive du manga, avec ajout de bande son, bruitages et doublages sur le
                manga adapté du visual novel Umineko no Naku Koro ni.</p>

            <Typography component="h2" variant="h4">Fonctionnement du lecteur</Typography>
            <p>Sur mobile :</p>
            <ul>
                <li>Appuyer sur la moitié gauche de l’écran pour aller à la page suivante (un manga se lit de droite à gauche), et inversement sur la moitié droite pour revenir en arrière.</li>
                <li>Écarter deux doigts pour zoomer, pincer l’écran pour dézoomer.</li>
            </ul>
            <p>Sur ordinateur :</p>
            <ul>
                <li>Utiliser la flèche gauche du clavier pour aller à la page suivante (un manga se lit de droite à gauche), et inversement la flèche droite pour revenir en arrière. Alternative : cliquer sur la moitié gauche de l’écran pour aller à la page suivante, et inversement sur la moitié droite pour revenir en arrière.</li>
                <li>Pour zoomer, utilisez la molette du clavier ou un double clic. Pour dézoomer, la molette en sens inverse.</li>
            </ul>

            <p>Note : l’URL de votre navigateur se met à jour à chaque changement de page, donc gardez-la en favori pour reprendre votre lecture à tout moment.</p>

            <Typography align="center" gutterBottom={true}>
                <Button size="large" color="primary" variant="contained" to="/episode-1/chapter-1" component={Link}>
                    Commencer la lecture
                </Button>
            </Typography>

            <Typography component="h2" variant="h4">En cas de problème de lecture audio</Typography>
            <p>L’audio ne fonctionne actuellement qu’avec des navigateurs modernes <MuiLink href="https://caniuse.com/#feat=ogg-vorbis">supportant
                le format audio ogg vorbis</MuiLink>. Pour faire court, n’utilisez pas Safari ou Internet Explorer 11. C’est OK avec Firefox ou les dérivés de Chromium (Chrome, Edge, Opera, Brave, etc)</p>
            <p>Si vous continuez à rencontrer des problèmes de lecture avec ces navigateurs, notamment sur la première page&nbsp;:</p>

            <Typography component="h2" variant="h4">En cas de problème de chargement des images</Typography>
            <p>Une connexion Internet <strong>rapide</strong> et <strong>stable</strong> est <ins>nécessaire</ins>. Pour les connexions plus lentes, des pistes d’amélioration (pas encore disponibles) sont envisagées telles que le préchargement des pages ou une version hors ligne.</p>

            <Grid container spacing={2}>
                <Grid item lg={4}>
                    <Typography component="h3" variant="h5">Mozilla Firefox</Typography>
                    <p>Vous ne pourrez réaliser cette manipulation qu’après avoir accédé une première fois au lecteur
                        (bouton « Commencer la lecture » ci-dessous).</p>
                    <img src="/img/allow_media_firefox.jpg" style={{maxWidth: "100%"}}
                         alt="Cliquer sur l’icône à gauche de la barre d’adresses et dans les permissions Lire automatiquement des médias, choisir Autoriser"
                    />
                </Grid>
                <Grid item lg={8}>
                    <Typography component="h3" variant="h5">Google Chrome</Typography>
                    <img src="/img/allow_media_chrome.jpg" style={{maxWidth: "100%"}}
                         alt="Cliquer sur l’icône à gauche de la barre d’adresses puis sur Paramètres de site. Dans les permissions Son, choisir Autoriser"
                    />
                </Grid>
            </Grid>

            <hr />

            <Typography component="h3" variant="h5">Épisodes</Typography>
            <Typography component="h4" variant="h6">Épisode 1 | Legend of the Golden Witch</Typography>
            <p>Histoire par Ryūkishi07, dessins par Kei Natsumi, traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink>.</p>
            <GridList className={classes.gridList} cellHeight={300} cols={4}>
                {[...Array(4)].map((x, i) => (
                    <GridListTile key={1 + "-" + (i + 1)} cols={1} style={{textAlign: "center"}}>
                        <UminekoCover episode={1} volume={i + 1} />
                    </GridListTile>
                ))}
            </GridList>

            <Typography component="h4" variant="h6">Épisode 2 | Turn of the Golden Witch</Typography>
            <p>Histoire par Ryūkishi07, dessins par Jirō Suzuki, traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink>.</p>
            <GridList className={classes.gridList} cellHeight={300} cols={5}>
                {[...Array(5)].map((x, i) => (
                    <GridListTile key={2 + "-" + (i + 1)} cols={1} style={{textAlign: "center"}}>
                        <UminekoCover episode={2} volume={i + 1} />
                    </GridListTile>
                ))}
            </GridList>

            <Typography component="h4" variant="h6">Épisode 3 | Banquet of the Golden Witch</Typography>
            <p>Histoire par Ryūkishi07, dessins par Kei Natsumi, traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink>.</p>
            <GridList className={classes.gridList} cellHeight={300} cols={5}>
                {[...Array(5)].map((x, i) => (
                    <GridListTile key={3 + "-" + (i + 1)} cols={1} style={{textAlign: "center"}}>
                        <UminekoCover episode={3} volume={i + 1} />
                    </GridListTile>
                ))}
            </GridList>

            <Typography component="h4" variant="h6">Épisode 4 | Alliance of the Golden Witch</Typography>
            <p>Histoire par Ryūkishi07, dessins par Sōichirō, traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink>.</p>
            <GridList className={classes.gridList} cellHeight={300} cols={6}>
                {[...Array(6)].map((x, i) => (
                    <GridListTile key={4 + "-" + (i + 1)} cols={1} style={{textAlign: "center"}}>
                        <UminekoCover episode={4} volume={i + 1} />
                    </GridListTile>
                ))}
            </GridList>

            <Typography component="h4" variant="h6">Épisode 5 | End of the Golden Witch</Typography>
            <p>Histoire par Ryūkishi07, dessins par Akitaka, traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink>.</p>
            <GridList className={classes.gridList} cellHeight={300} cols={6}>
                {[...Array(6)].map((x, i) => (
                    <GridListTile key={5 + "-" + (i + 1)} cols={1} style={{textAlign: "center"}}>
                        <UminekoCover episode={5} volume={i + 1} />
                    </GridListTile>
                ))}
            </GridList>

            <Typography component="h4" variant="h6">Épisode 6 | Dawn of the Golden Witch</Typography>
            <p>Histoire par Ryūkishi07, dessins par Hinase Momoyama, traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink> jusqu’au chapitre 14, disponible en anglais à partir du chapitre 15.</p>
            <GridList className={classes.gridList} cellHeight={300} cols={6}>
                {[...Array(6)].map((x, i) => (
                    <GridListTile key={6 + "-" + (i + 1)} cols={1} style={{textAlign: "center"}}>
                        <UminekoCover episode={6} volume={i + 1} />
                    </GridListTile>
                ))}
            </GridList>

            <Typography component="h4" variant="h6">Épisode 7 | Requiem of the Golden Witch</Typography>
            <p>Histoire par Ryūkishi07, dessins par Eita Mizuno, uniquement disponible en anglais.</p>
            <GridList className={classes.gridList} cellHeight={300} cols={9}>
                {[...Array(9)].map((x, i) => (
                    <GridListTile key={7 + "-" + (i + 1)} cols={1} style={{textAlign: "center"}}>
                        <UminekoCover episode={7} volume={i + 1} />
                    </GridListTile>
                ))}
            </GridList>
        </Layout>
    );
}

export default Homepage;