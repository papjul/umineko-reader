import React from "react";
import Layout from "../containers/Layout";
import {Link, useParams} from "react-router-dom";
import {Link as MuiLink} from "@material-ui/core";
import _ from "lodash";
import episodes from "../data/episodes";
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    cellSuccess: {
        backgroundColor: "#4caf50",
    },
    cellWarning: {
        backgroundColor: "#ff9800",
    },
    cellError: {
        backgroundColor: "#f44336",
    },
}));

const EpisodeView = () => {
    const classes = useStyles();
    const params = useParams();
    const episode = parseInt(params.episode);

    // TODO: Titre des chapitres
    return (
        <Layout>
            <h2>Épisode {episode} | {episodes[episode].title}</h2>
            <p>Histoire par Ryūkishi07, dessins par {episodes[episode].art}, {episode <= 6 &&
            <>traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink></>}{episode === 6 && " jusqu’au chapitre 14, disponible en anglais à partir du chapitre 15"}{episode === 7 &&
            <>uniquement disponible en anglais</>}{episode === 8 &&
            <>traduction en anglais par <MuiLink href="https://wtdnd.wordpress.com/">WTDND</MuiLink></>}.</p>
            <ul>
                {!_.isEmpty(episodes[episode].volumes) && Object.values(episodes[episode].volumes).map((chapters) => (
                    chapters.map((chapter) => (
                        <li key={chapter}>
                            <Link to={"/episode-" + episode + "/chapter-" + chapter}
                                  component={MuiLink}>Chapitre {chapter}</Link>
                        </li>
                    ))
                ))}
            </ul>

            <h2>Avancement</h2>
            <TableContainer component={Paper}>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Chapitre</TableCell>
                            <TableCell>Page</TableCell>
                            <TableCell>Musique</TableCell>
                            <TableCell>Effets</TableCell>
                            <TableCell>Doublage</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {_.map(episodes[episode].chapters, (pages, chNumber) => {
                            return <>
                                {_.map(episodes[episode].chapters[chNumber], (page, pageNumber) => {
                                    const failClass = (_.isEmpty(page.se) && !page.bgm && !page.voice) ? classes.cellError : classes.cellWarning;
                                    return <TableRow>
                                        <TableCell>{chNumber}</TableCell>
                                        <TableCell>{pageNumber + 1}</TableCell>
                                        <TableCell className={page.bgm ? classes.cellSuccess : failClass}>{page.bgm}</TableCell>
                                        <TableCell className={_.isEmpty(page.se) ? failClass : classes.cellSuccess}>{JSON.stringify(page.se)}</TableCell>
                                        <TableCell className={page.voice ? classes.cellSuccess : failClass}>{page.voice ? "Oui" : "Non"}</TableCell>
                                    </TableRow>;
                                })}
                            </>;
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Layout>
    );
};

export default EpisodeView;