import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router-dom";
import Reader from "../components/Reader";
import Layout from "../containers/Layout";

const ChapterView = () => {
    const params = useParams();

    const [script, setScript] = useState(null);

    const episode = params.episode;

    useEffect(() => {
        fetch('/assets/ep-' + episode + '/script.json')
            .then(response => response.json())
            .then(data => setScript(data))
            .catch((err) => {
                setScript(null);
                console.log(err);
            });
    }, [episode]);

    return script ?
        <Reader script={script} />
        : <Layout>
            <div>Chargement…</div>
        </Layout>;
};

export default ChapterView;