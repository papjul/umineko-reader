import React from "react";
import Layout from "../containers/Layout";
import {Link, useParams} from "react-router-dom";
import {Link as MuiLink} from "@material-ui/core";
import episodes from "../data/episodes";

const VolumeView = () => {
    const params = useParams();
    const episode = parseInt(params.episode);
    const volume = parseInt(params.volume);

    return (
        <Layout>
            <h2>Épisode {episode} | {episodes[episode].title} | Tome {volume}</h2>
            <p>Histoire par Ryūkishi07, dessins par {episodes[episode].art}, {episode <= 6 &&
            <>traduction en français par <MuiLink
                href="https://miammiam-team.org/">MiamMiamTeam</MuiLink></>}{episode === 6 && " jusqu’au chapitre 14, disponible en anglais à partir du chapitre 15"}{episode === 7 &&
            <>uniquement disponible en anglais</>}{episode === 8 &&
            <>traduction en anglais par <MuiLink href="https://wtdnd.wordpress.com/">WTDND</MuiLink></>}.</p>
            <ul>
                {episodes[episode].volumes[volume] && episodes[episode].volumes[volume].length > 0 && episodes[episode].volumes[volume].map((chapter) => (
                    <li key={chapter}>
                        <Link to={"/episode-" + episode + "/chapter-" + chapter} component={MuiLink}>
                            Chapitre {chapter}
                        </Link>
                    </li>
                ))}
            </ul>
        </Layout>
    );
};

export default VolumeView;